# Keep Me Informed

A project updater for everyone.

## Why ?

🚀 At Nexylan, our projects size is continuously growing.

⭐ This is why we have to [eliminate as TOIL][toil] as possible to focus on making quality products.

💪 This tool will help us, and you, with that by doing a global update of your project for you.

💭 Combined with [nexylan/ci][ci], you will never think about project updates again.

[toil]: https://landing.google.com/sre/sre-book/chapters/eliminating-toil/ "Eliminating Toil"
[ci]: https://gitlab.com/nexylan/ci

## Installation

You have to enable GitLab CI on your project.

Also, you have to setup a `GITLAB_TOKEN` to make jobs working correctly.
More info: https://gitlab.com/nexylan/ci#the-ci-cant-access-to-my-repository

## Usage

This project is composed by multiple ready to use GitLab CI configurations
to be included according to your project structure.

Currently, only node is supported with npm and yarn.
Contributions to support more languages and packager are very welcomed! 👍

Here is an setup example:

```yaml
include:
  - { project: nexylan/kmi, ref: vX.Y.Z, file: ci/<CONFIG>.gitlab-ci.yml }
```

It will create a new named job on the **maintain** stage.

See below for job type details.

You may add extra configuration trough environment variables:

- `NEXY_KMI_PROJECT_DIR`: Project directory where to perform the update instructions.

### Node (npm + yarn)

- file: `ci/node.gitlab-ci.yml`
- job name: `update:node`

If npm is detected as a package manager, then a `npm audit fix` is run before `npm update`.

If you want to skip the audit part, you can configure your CI like this:

```yaml
include:
  - { project: nexylan/kmi, ref: vX.Y.Z, file: ci/node.gitlab-ci.yml }

variables:
  NEXY_KMI_NODE_SKIP_AUDIT: 1
```

This is useful when your project framework, such as Expo, needs to have some constraint locked to a fixed version.

### Custom job using template

Creating custom automatic update jobs is very easy but required a few more configuration.

First, include the `ci/template.gitlab-ci.yml` file on top of your GitLab CI configuration:

```yaml
include:
  - { project: nexylan/kmi, ref: vX.Y.Z, file: ci/template.gitlab-ci.yml }
```

Then, configure your custom job that will extends `.kmi:template`:

```yaml
update:
  extends: .kmi:template
  variables:
    NEXY_KMI_BRANCH_NAME: update/app
    NEXY_KMI_COMMIT_MESSAGE: "fix(app): updates"
  script:
    - echo "Your custom update logic here."
```

And that's it ! You don't have to worry about how the merge request will be created, KMI handle the rest.

#### Custom docker image

The default used image is a
[nexylan custom alpine image](https://gitlab.com/nexylan/docker/core)
where we install the necessary tools for KMI.

You may need to change the image to fit your update logic. This can be easily with the `image` section:

```yaml
update:
  extends: .kmi:template
  image: registry.gitlab.com/my/custom-image:custom-tag
```

⚠️ Your image MUST be based from Debian OS to work.
The current version of KMI need this to the the provided `apt-get` package manager.

If you want KMI to support an another variant, please open an issue.

#### Multiple update jobs

In case of complex projects with multiple and slightly different update logic,
you may want to separate them onto different job.

To do so, just define multiple jobs using the `.kmi:template`:

```yaml
update:app:
  extends: .kmi:template
  variables:
    NEXY_KMI_BRANCH_NAME: update/app
  script:
    - APP_VERSION="v1.2.3"
    - 'export NEXY_KMI_COMMIT_MESSAGE="fix(app): update to v${APP_VERSION}"'
    - echo "Your custom update logic here."

update:license:
  extends: .kmi:template
  variables:
    NEXY_KMI_BRANCH_NAME: update/license
    NEXY_KMI_COMMIT_MESSAGE: "docs: license update"
  script:
    - curl https://spdx.org/licenses/MIT.txt > LICENSE
```

⚠️ You MUST define **at least** different branch name to make it fully working.
You SHOULD differentiate the commit messages too to make your merge requests list easier to read.

This allows you to get and merge the result separately making the maintenance job easier.
